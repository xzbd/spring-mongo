package com.xzbd.springmongo.common.http;


import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;


public class ApiResultUtil {
    /**
     * 请求成功无数据返回
     *
     * @return
     */
    public static ResponseEntity<Object> success() {
        return success(null);
    }

    /**
     * 请求成功
     *
     * @return
     */
    public static ResponseEntity<Object> success(Object data) {
        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(true);
        apiResult.setData(data);
        apiResult.setCode("200");
        apiResult.setMessage("操作成功");
        return getEntity(apiResult);
    }
    public static ResponseEntity<Object> success(Object data,String message ){
        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(true);
        apiResult.setData(data);
        apiResult.setCode("200");
        apiResult.setMessage(message == null ? "" : message );
        return getEntity(apiResult);
    }


    /**
     * 用户发出的请求有错误，服务器没有进行新建或修改数据的操作，该操作是幂等的。
     *
     * @return
     */
    public static ResponseEntity<Object> invalidRequest(String msg) {
        return buildApiResult("400", msg == null ? "INVALID REQUEST" : msg);
    }

    /**
     * 表示用户没有权限（令牌、用户名、密码错误）
     *
     * @return
     */
    public static ResponseEntity<Object> unauthorized(String msg) {
        return buildApiResult("401", msg == null ? "Unauthorized" : msg);
    }

    /**
     * 表示用户得到授权（与401错误相对），但是访问是被禁止的。
     *
     * @return
     */
    public static ResponseEntity<Object> forbidden(String msg) {
        return buildApiResult("403", msg == null ? "Forbidden" : msg);
    }

    /**
     * 用户发出的请求针对的是不存在的记录，服务器没有进行操作，该操作是幂等的。
     *
     * @return
     */
    public static ResponseEntity<Object> notFound(String msg) {
        return buildApiResult("404", msg == null ? "NOT FOUND" : msg);
    }

    /**
     * 用户请求的格式不可得(请求格式不对)。
     *
     * @return
     */
    public static ResponseEntity<Object> notAcceptable(String msg) {
        return buildApiResult("406", msg == null ? "Not Acceptable" : msg);
    }

    /**
     * 服务器发生错误，用户将无法判断发出的请求是否成功。
     *
     * @return
     */
    public static ResponseEntity<Object> internalServerError(String msg) {
        return buildApiResult("500", msg == null ? "INTERNAL SERVER ERROR"
                : msg);
    }

    /**
     * 自定义错误返回
     *
     * @param code
     * @param message
     * @return
     */
    public static ResponseEntity<Object> error(String code, String message) {
        return buildApiResult(code, message);
    }

    private static ResponseEntity<Object> buildApiResult(String code,
                                                             String message) {
        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(false);
        apiResult.setCode(code);
        apiResult.setMessage(message);
        return getEntity(apiResult);
    }

    private static ResponseEntity<Object> getEntity(Object data) {
        MultiValueMap<String, String> headers = new HttpHeaders();
        List<String> contentType = new ArrayList<String>();
        contentType.add("application/json;charset=utf-8");
        headers.put("Content-Type", contentType);
        // 所有接口HttpStatus统一返回200,客户端使用success字段来判断请求是否成功.
        return new ResponseEntity<Object>(data, headers, HttpStatus.OK);
    }
}

