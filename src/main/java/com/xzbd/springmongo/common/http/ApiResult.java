package com.xzbd.springmongo.common.http;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ApiResult {
    private boolean success;
    private Object data;
    private String message;
    private String code;
    
    public ApiResult() {
    }

    public ApiResult(boolean success, Object data,String message,String code) {
        this.success = success;
        this.data = data;
        this.message = message;
        this.code = code;
    }

    
}
