package com.xzbd.springmongo.controller;

import java.util.Objects;
import java.util.Optional;

import com.xzbd.springmongo.common.http.ApiResultUtil;
import com.xzbd.springmongo.domain.User;
import com.xzbd.springmongo.service.UserService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "用户管理服务", tags = "Swagger用户信息管理API")
@RestController
@RequestMapping("/user")
public class UserController {
  @Autowired
  private UserService userService;
  
  @ApiOperation(value = "新增保存用户")
  @PostMapping
  public ResponseEntity<Object> save(@RequestBody User user) {
    User save = userService.saveUser(user);
    return ApiResultUtil.success(save);
  }

  @ApiOperation("修改用户")
  @PutMapping
  public ResponseEntity<Object> update(@RequestBody User user) {
    User update = userService.updatUser(user);;
    return ApiResultUtil.success(update);
  }

  @ApiOperation("删除用户")
  @DeleteMapping("/{id}")
  public ResponseEntity<Object> delete(@PathVariable("id") String id ) {
    userService.deleteUser(id);
   return ApiResultUtil.success(); 
  }
 
  @ApiOperation("根据id查找用户")
  @GetMapping("/{id}")
  public ResponseEntity<Object> findOne(@PathVariable("id") String id){
    Optional<User> findUser = userService.findUser(id);
    if(findUser.isPresent()){
      return ApiResultUtil.success(findUser.get());
    }else{
      return ApiResultUtil.notFound("用户不存在");
    }
  }

  @ApiOperation("根据条件查询用户列表-分页")
  @PostMapping("/page")
  public ResponseEntity<Object> page(@RequestBody User user,Integer page, Integer pageSize) {
    Pageable pageable = PageRequest.of(page, pageSize);
    Query query = new Query();

    Criteria criteria = new Criteria();
    if(StringUtils.isNotBlank(user.getId())){
      criteria.and("_id").is(user.getId());
    }
    if(StringUtils.isNotBlank(user.getName())){
      criteria.and("name").is(user.getName());
    }
    if(Objects.nonNull(user.getAge())){
      criteria.and("age").is(user.getAge());
    }
    query.addCriteria(criteria);
        
    Page<User>  pageUser = userService.pageUser(pageable, query);
    return ApiResultUtil.success(pageUser);
  }


}
