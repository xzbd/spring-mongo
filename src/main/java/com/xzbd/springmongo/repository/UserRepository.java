package com.xzbd.springmongo.repository;

import com.xzbd.springmongo.common.core.BaseRepository;
import com.xzbd.springmongo.domain.User;

import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User,String> {
  
}
