package com.xzbd.springmongo.service;

import java.util.Optional;

import com.xzbd.springmongo.domain.User;
import com.xzbd.springmongo.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public User saveUser(User user) {
    return userRepository.save(user);
  }

  @Override
  public void deleteUser(String id) {
    userRepository.deleteById(id);
  }

  @Override
  public User updatUser(User user) {
    return userRepository.save(user);
  }

  @Override
  public Optional<User> findUser(String id) {
    return userRepository.findById(id);
  }

  @Override
  public Page<User> pageUser(Pageable pageable, Query query) {
    return userRepository.findAll(mongoTemplate, pageable, query, User.class);
  }
  
}
