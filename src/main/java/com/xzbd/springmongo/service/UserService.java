package com.xzbd.springmongo.service;

import java.util.List;
import java.util.Optional;

import com.xzbd.springmongo.domain.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;

public interface UserService {

  User saveUser(User user);

  void deleteUser(String id);

  User updatUser(User user);

  Optional<User> findUser(String id);

  Page<User> pageUser(Pageable pageable,Query query);


}
