package com.xzbd.springmongo.domain;

import com.xzbd.springmongo.common.core.BaseDocument;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class User extends BaseDocument {
  @Id
	private String id;
  private String name;
  private int age;
}
