# spring-mongo

#### 介绍

该项目是一个练习项目，使用了`spring-boot:2.5.0` 、`mongoDB` 、`swagger-ui3`
swagger-ui 访问地址 `localhost:8888/swagger-ui/`

#### 软件架构

- spring-boot 2.5.0
- mongoDB
- swagger-ui3

#### 使用说明

1.  clone 该项目
2.  安装 mongoDB 数据库，对应的 docker 镜像： `xzbd/mongo-server`
3.  访问 `http://localhost:8888/swagger-ui/` ，通过 swagger 测试接口

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
