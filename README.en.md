# spring-mongo

#### Description

该项目是一个练习项目，使用了`spring-boot:2.5.0` 、`mongoDB` 、`swagger-ui3`
swagger-ui 访问地址 `localhost:8888/swagger-ui/`

#### Software Architecture

- spring-boot 2.5.0
- mongoDB
- swagger-ui3

#### Installation

1.  clone 该项目
2.  安装 mongoDB 数据库，对应的 docker 镜像： `xzbd/mongo-server`
3.  访问 `http://localhost:8888/swagger-ui/` ，通过 swagger 测试接口

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
